FROM python:3.8-slim

COPY ./gitlab-issues-sync.py /run/gitlab-issues-sync.py

RUN pip install colored argparse python-gitlab requests
RUN chmod +x /run/gitlab-issues-sync.py && \
    ln -s /run/gitlab-issues-sync.py /usr/bin/gitlab-issues-sync

CMD ["gitlab-issues-sync"]
